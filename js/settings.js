function validateForm() {
    var form = document.getElementById('contact-form');
    var inputs = form.getElementsByTagName('input');
  
    for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].hasAttribute('required') && !inputs[i].value) {
        alert('Please fill in all required fields.');
        return false;
      }
    }
  
    form.submit();
  }