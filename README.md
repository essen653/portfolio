# Portfolio
Full Stack Developer


## About Me
I am a web developer with a vast array of knowledge in many different front end & back end languages, responsive frameworks, databases, and best code practices.
                                        

## My Stack
Design.

- [ ] [Figma](#)

Frontend.

- [ ] [Html5](#)
- [ ] [CSS](#)
- [ ] [Js](#)


Backend.

- [ ] [PHP](#)
- [ ] [Python](#)

FrameWork.

- [ ] [Boostrap5](#)
- [ ] [Custom PHP](#)
- [ ] [Django Python](#)

FrameWork.

- [ ] [MySql](#)
- [ ] [MongoDB](#)

***

## License
Source code is open for everyone.

## Contact
Email: itzy@pm.me
Phone: 08163650608

